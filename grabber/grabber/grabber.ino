#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <TimerOne.h>

// OLED display TWI address
#define OLED_ADDR 0x3C

#define BUTTON_PIN 3
#define SEND_PIN 10

Adafruit_SSD1306 display(128, 64);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif
#define RX_PIN 2
#define BUTTON_PIN 3

volatile const unsigned int lo_duration = 700; // mcs
volatile const unsigned int hi_duration = 1400; // mcs
volatile const unsigned int delta = 300; // mcs
volatile bool gotCode = false;

volatile unsigned long low_period = 0;
volatile unsigned long hi_period = 0;
volatile unsigned long last_micros = 0;
volatile unsigned long last_display_millis = 0;
volatile unsigned long last_button_millis = 0;
volatile unsigned int counter = 0;
volatile unsigned int code = 0;
volatile unsigned long state = LOW;
volatile byte displayCounter = 1;
volatile bool inProgress = false;

const unsigned long LongPeriod = 1000; // 1 sec
const unsigned long ShortPeriod = 400; //0.3 sec

typedef enum Mode {
  Scanning,
  Sending
};

volatile Mode mode = Scanning;

void send0();
void send1();

void setup() {
  Serial.begin(1000000);

  // initialize and clear display
  display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
  display.clearDisplay();

  // display a line of text
  display.setTextSize(1);
  display.setTextColor(WHITE);
  
  display.display();
  
  pinMode(RX_PIN, INPUT);
  digitalWrite(RX_PIN, LOW);
  attachInterrupt(0, grabCode, CHANGE);
  attachInterrupt(1, buttonClicked, CHANGE);

  pinMode(SEND_PIN, OUTPUT);
  digitalWrite(SEND_PIN, LOW);

  // включить таймер
  //Timer1.initialize(200000); // 0.2 секунды
  //Timer1.attachInterrupt(Timer1_action);
  
  Serial.println("setup completed");
}

void loop() {

  if (inProgress) {

    for (int i = 0; i < 10; i++) {
      delay(25);
//      Serial.println("send 1");
      sendCode();
    }

    inProgress = false;
  }
  
  display.clearDisplay();

//  display a pixel in each corner of the screen
  display.drawPixel(0, 0, WHITE);
  display.drawPixel(127, 0, WHITE);
  display.drawPixel(0, 63, WHITE);
  display.drawPixel(127, 63, WHITE);

  display.setCursor(20,10);
  if (code == 0) {
    display.print("Last Code Unknown");
  }
  else {
    display.print("Last Code " + String(code));
  }
  display.setCursor(10,30);
  
  if (mode == Scanning) {
    
    String scanning = "Scanning.  ";
    if (displayCounter == 2) {
      scanning = "Scanning.. ";
    }
    else if (displayCounter == 3) {
      scanning = "Scanning...";
    }

    if (++displayCounter > 3) {
      displayCounter = 1;
    }

    //Serial.println(scanning);
    display.print(scanning);
  }
  else if (mode == Sending) {
    String sending = "Sending.  ";
    if (displayCounter == 2) {
      sending = "Sending.. ";
    }
    else if (displayCounter == 3) {
      sending = "Sending...";
    }

    if (++displayCounter > 3) {
      displayCounter = 1;
    }

    //Serial.println(scanning);
    display.print(sending);
  }

  // update display with all of the above graphics
  display.display();
  delay(500);

  last_display_millis = millis();
  //Timer1_action();
}

// return 0 if 0
// return 1 if 1
// return -1 if unknown

// Длительности импульсов NICE:
// Лог. «1» – 1400мкс низкий уровень (два интервала), 700мкс высокий (один интервал)
// Лог. «0» – 700мкс низкий уровень (один интервал), 1400мкс высокий (два интервала).
// Пилотный период – 25200мкс , стартовый импульс – 700 мкс.
// http://phreakerclub.com/1615

int is0Or1() {

  int delta1 = low_period - lo_duration;
  int delta2 = hi_period - hi_duration;
  int delta3 = low_period - hi_duration;
  int delta4 = hi_period - lo_duration;

  delta1 = abs(delta1);
  delta2 = abs(delta2);
  delta3 = abs(delta3);
  delta4 = abs(delta4);
  
  if (delta1 <= delta) {
    if (delta2 <= delta) {
      return 0;
    }
  }
  
  if (delta3 <= delta) {
    if (delta4 <= delta) {
      return 1;
    }
  }
  
  return -1;
}

void buttonClicked() {

  if (inProgress) {
    return;
  }

  unsigned long button_millis = millis();
  unsigned long delta = button_millis - last_button_millis;
  if (delta > 300 && digitalRead(BUTTON_PIN) == LOW) {
    Serial.println("button clicked");

    inProgress = true;
    last_button_millis = button_millis;
  }
}

void grabCode() {

  //Serial.println("grabCode");
  if (gotCode) {
    return;
  }

  state = digitalRead(RX_PIN);
  unsigned long current_micros = micros();
  
  if (state == HIGH) {
    low_period = current_micros - last_micros;
  }
  else {
    hi_period = current_micros - last_micros;
  }
  
  last_micros = current_micros;
  
  if (state == LOW) {
    int result = is0Or1();
    
    if (result == 0) {
      code = (code << 1) | 0;
      counter += 1;
      
      //Serial.println("This is 0");
    }
    else if (result == 1) {
      code = (code << 1) | 1;
      counter += 1;

      //Serial.println("This is 1");
    }
    else {
      code = 0;
      counter = 0;

      //Serial.println("This is an Error");
    }
  }
  
  if (counter == 12) {
    Serial.println("code scanned");
     gotCode = true;
  }
}

// функиця для генерирования кода
// пилотный период + 1 + код + 01

// Длительности импульсов NICE:
// Лог. «1» – 1400мкс низкий уровень (два интервала), 700мкс высокий (один интервал)
// Лог. «0» – 700мкс низкий уровень (один интервал), 1400мкс высокий (два интервала).
// Пилотный период – 25200мкс , стартовый импульс – 700 мкс.
// http://phreakerclub.com/1615
void sendCode() {
  
  // pilot period
  digitalWrite(SEND_PIN, LOW);
  delayMicroseconds(25200);
  //delay(20);
  delayMicroseconds(700);

  // log 1
  send1();

//-------------------
//  send0();
//  send0();
//  send0();
//  send0();
//  send0();
//  send0();
//  send0();
//  send0();
//  send0();
//  send0();
//  send0();
//------------------
  // code
  unsigned short tempCode = code;
  tempCode = tempCode << 3;
  for (int i = 0; i < 10; i++) {
    tempCode = tempCode << 1;
    unsigned short lastBit = tempCode & 0x8000; // 1000.0000.0000.0000b

    if (lastBit == 0) {
      send0();
    }
    else {
      send1();
    }
  }

  // log 0
  send0();

  // log 1
  send1();
}

void send0() {
  digitalWrite(SEND_PIN, LOW);
  delayMicroseconds(700);
  digitalWrite(SEND_PIN, HIGH);
  delayMicroseconds(1400);

  //Serial.println("bit 0");
}

void send1() {
  digitalWrite(SEND_PIN, LOW);
  delayMicroseconds(1400);
  digitalWrite(SEND_PIN, HIGH);
  delayMicroseconds(700);
  //Serial.println("bit 1");
}



void sendTest() {
  digitalWrite(SEND_PIN, LOW);
  delayMicroseconds(10000);
  digitalWrite(SEND_PIN, HIGH);
  delayMicroseconds(10000);

  //Serial.println("bit 0");
}
