ddd#include "tripod.h"
#include <Timer1.h>

const int steppe1_Pin1 = 7;
const int steppe1_Pin2 = 6;
const int steppe1_Pin3 = 5;
const int steppe1_Pin4 = 4;

const int steppe2_Pin1 = 8;
const int steppe2_Pin2 = 9;
const int steppe2_Pin3 = 10;
const int steppe2_Pin4 = 11;

bool toLeft = false;
bool toRight = false;
bool toUp = false;
bool toDown = false;
int val1 = 0;
int val2 = 0;

stepper s1 = {0, 0};
stepper s2 = {0, 0};

int bluetoothCounter = 50;
int incomingByte = 0;

void setup()
{
  /*pinMode(steppe1_Pin1, OUTPUT);
  pinMode(steppe1_Pin2, OUTPUT);
  pinMode(steppe1_Pin3, OUTPUT);
  pinMode(steppe1_Pin4, OUTPUT);
  
  digitalWrite(steppe1_Pin1, LOW);
  digitalWrite(steppe1_Pin2, LOW);
  digitalWrite(steppe1_Pin3, LOW);
  digitalWrite(steppe1_Pin4, LOW);
  
  pinMode(steppe2_Pin1, OUTPUT);
  pinMode(steppe2_Pin2, OUTPUT);
  pinMode(steppe2_Pin3, OUTPUT);
  pinMode(steppe2_Pin4, OUTPUT);
  
  digitalWrite(steppe2_Pin1, LOW);
  digitalWrite(steppe2_Pin2, LOW);
  digitalWrite(steppe2_Pin3, LOW);
  digitalWrite(steppe2_Pin4, LOW);*/
  
  DDRD = 0xFF;
  DDRB = 0xFF;
  
  Serial.begin(9600);
  startTimer1(1500);
}

ISR(timer1Event)
{
  resetTimer1();
  
  if (toLeft)
  {
    s1.current_step++;
  }
  if (toRight)
  {
    s1.current_step--;
  }
  if (toUp)
  {
    s2.current_step++;
  }
  if (toDown)
  {
    s2.current_step--;
  }
  
  next_step(&s1, &s2);
  
  if (incomingByte > 0 && --bluetoothCounter <= 0)
  {
    incomingByte = 0;
    toLeft = false;
    toRight = false;
    toUp = false;
    toDown = false;
    
    bluetoothCounter = 50;
  }
}

void next_step(stepper *p_stepper1, stepper *p_stepper2)
{
  unsigned int s1 =
   p_stepper1->current_step - p_stepper1->current_step / 4 * 4;
  unsigned int s2 =
   p_stepper2->current_step - p_stepper2->current_step / 4 * 4;
   
   unsigned char mask1 = 0;
   switch (s1)
  {
    case 0:
      mask1 |= stepper1_input1;
      break;
    case 1:
      mask1 |= stepper1_input2;
      break;
    case 2:
      mask1 |= stepper1_input3;
      break;
    case 3:
      mask1 |= stepper1_input4;
      break;
  };
  
  unsigned char mask2 = 0;
   switch (s2)
  {
    case 0:
      mask2 |= stepper2_input1;
      break;
    case 1:
      mask2 |= stepper2_input2;
      break;
    case 2:
      mask2 |= stepper2_input3;
      break;
    case 3:
      mask2 |= stepper2_input4;
      break;
  };
  
  PORTD = mask1;
  PORTB = mask2;
}

void loop()
{
  if (incomingByte > 0)
    return;
    
  val1 = analogRead(17);
  val2 = analogRead(18);
  
  toLeft = (val1 < 400);
  toRight = (val1 > 750);
  if ((val1 < 200) || (val1 > 900))
    s1.speed = 2;
  else if ((val1 < 400) || (val1 > 750))
    s1.speed = 1;
  else
    s1.speed = 0;
   
  toUp = (val2 < 400);
  toDown = (val2 > 750);
  if ((val1 < 200) || (val1 > 900))
    s2.speed = 2;
  else if ((val1 < 400) || (val1 > 700))
    s2.speed = 1;
  else
    s2.speed = 0;
    
  if (Serial.available() > 0)
  {
    incomingByte = Serial.read();
    
    Serial.print("I received: ");
    Serial.println(incomingByte, DEC);
  }
  
  if (incomingByte == 97) // left
    toLeft = true;
  if (incomingByte == 100) // right
    toRight = true;
  if (incomingByte == 119) // up
    toUp = true;
  if (incomingByte == 115)  // down
    toDown = true;
    
  if (toLeft)
    Serial.println("toleft");
  if (toRight)
    Serial.println("toright");
  if (toDown)
    Serial.println("todown");
  if (toUp)
    Serial.println("toup");  
}

