// shift register output bits
const unsigned char stepper1_input1 = 0xB0;
const unsigned char stepper1_input2 = 0xE0;
const unsigned char stepper1_input3 = 0x70;
const unsigned char stepper1_input4 = 0xD0;

// shift register output bits
const unsigned char stepper2_input1 = 0x0B;
const unsigned char stepper2_input2 = 0x0E;
const unsigned char stepper2_input3 = 0x07;
const unsigned char stepper2_input4 = 0x0D;

typedef struct
{
	unsigned int current_step;
	unsigned int speed;
} stepper;

//Step C0 C1 C2 C3
//1  1  0  1  1
//2  1  1  1  0
//3  0  1  1  1
//4  1  1  0  1
