// program 1 text
unsigned char program_1_expression[] = "this is text of program one";

// program 2 text
unsigned char program_2_expression[] = "this is text of program two";

// program 3 text
unsigned char program_3_expression[] = "this is text of program three";

// program 4_1 text
unsigned char program_4_1_expression[] = "this is text of program four one";

// program 4_2 text
unsigned char program_4_2_expression[] = "this is text of program four two";

// program 5_1 text
unsigned char program_5_1_expression[] = "this is text of program five one";

// program 5_2 text
unsigned char program_5_2_expression[] = "this is text of program five two";

// program 6 text
unsigned char program_6_expression[] = "this is text of program six";