class program_1_params
{
public:
  unsigned char * expression;
  unsigned int position;
  unsigned int beging_line;
  unsigned int height;

  char **matrix;
};

class program_4_params
{
public:
  unsigned char * expression_1;
  unsigned int beging_line_1;
  unsigned int height_1;
  unsigned int position_1;

  unsigned char * expression_2;
  unsigned int beging_line_2;
  unsigned int height_2;
  unsigned int position_2;

  char **matrix;
};

class program_5_params
{
public:
  unsigned char * expression_1;
  unsigned int beging_line_1;
  unsigned int height_1;
  unsigned int position_1;
  unsigned int speed_1;

  unsigned char * expression_2;
  unsigned int beging_line_2;
  unsigned int height_2;
  unsigned int position_2;
  unsigned int speed_2;

  char **matrix;
};

class program_6_params
{
public:
  unsigned char * expression;
  bool blink;

  unsigned int beging_line;
  unsigned int height;

  char **matrix;
};