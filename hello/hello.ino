//#include "expressions.h"
//#include "ru_small.h"

#include "en_expressions.h"
#include "en_small.h"
#include "template.h"
#include "structs.h"

#define COL_PIXEL 64
#define ROW_PIXEL 16

int latchPin = 8; //LT
int clockPin = 12; //SK
int dataPin = 11; //R1

int en_74138 = 2;
int la_74138 = 3;
int lb_74138 = 4;
int lc_74138 = 5;
int ld_74138 = 6;

unsigned int ROW_xPixel = 0;
unsigned int ROW_num = 0;
unsigned char Col_num_1 = 0;
unsigned char Col_num_2 = 0;
unsigned char Col_num_3 = 0;
unsigned char Col_num_4 = 0;
unsigned char Col_num_5 = 0;
unsigned char Col_num_6 = 0;
unsigned char Col_num_7 = 0;
unsigned char Col_num_8 = 0;

void shiftOut(unsigned char dataOut)
{
  for(int i=0;i<=7;i++)
  {
    PORTB &=~( 1<< (clockPin - 8)); //equate digitalWrite(clockPin, LOW);

    if (dataOut &(0x01 << i)) PORTB |= 1 << (dataPin - 8); //equate digitalWrite(dataPin, HIGH);
    else PORTB &= ~(1 << (dataPin - 8)) ;//equate digitalWrite(dataPin, LOW);

    PORTB |= 1 << (clockPin - 8); //equate digitalWrite(clockPin, HIGH);
  }
}

//Combine 2 bits/pixel to 1 bits/pixel 
unsigned char Combine_2BitsTo1Bit(unsigned char num,unsigned char *BMP)
{

  unsigned char Col_num_tem_1;
  unsigned char Col_num_tem_2;
  unsigned int Col_num_tem = 0;
  unsigned char i = 0;
  unsigned char Col_num_1bit = 0x00;

  Col_num_tem_1 = *(BMP + num);
  Col_num_tem_2 = *(BMP + num + 1);

  Col_num_tem = Col_num_tem_1;
  Col_num_tem |= (Col_num_tem_2 << 8);

  for (i = 0; i < 8; i++)
  {   
    if (Col_num_tem&(0x0003 << i * 2)) Col_num_1bit |= (0x01 << i);
  }

  return ~Col_num_1bit;
}

//display one picture 
void display_martix(unsigned char *BMP)
{
  //Display count
  unsigned int dis_cnt = 256;
  unsigned int i = 0;

  for (i = 0;i < /*dis_cnt * */16; i++)
  {
    digitalWrite(en_74138, HIGH); //Turn off display

    //Col scanning
    shiftOut(Col_num_1);
    shiftOut(Col_num_2);
    shiftOut(Col_num_3);
    shiftOut(Col_num_4);
    shiftOut(Col_num_5);
    shiftOut(Col_num_6);
    shiftOut(Col_num_7);
    shiftOut(Col_num_8);

    digitalWrite(latchPin, LOW);
    digitalWrite(latchPin, HIGH);

    // Row scanning
    // AVR Port Operation 
    PORTD = ((ROW_xPixel << 3 ) & 0X78) | (PORTD & 0X87); //Write PIN 3 4 5 6 la_74138 lb_74138 lc_74138 ld_74138

    digitalWrite(en_74138, LOW); //Turn on display

    if (ROW_xPixel == 15) ROW_xPixel = 0; 
    else ROW_xPixel++;

    // Single color,2 bits/pixel
    Col_num_1 = Combine_2BitsTo1Bit((COL_PIXEL / 8) * ROW_xPixel * 2, BMP);
    Col_num_2 = Combine_2BitsTo1Bit((COL_PIXEL / 8) * ROW_xPixel * 2 + 2, BMP);
    Col_num_3 = Combine_2BitsTo1Bit((COL_PIXEL / 8) * ROW_xPixel * 2 + 4, BMP);
    Col_num_4 = Combine_2BitsTo1Bit((COL_PIXEL / 8) * ROW_xPixel * 2 + 6, BMP);
    Col_num_5 = Combine_2BitsTo1Bit((COL_PIXEL / 8) * ROW_xPixel * 2 + 8, BMP);
    Col_num_6 = Combine_2BitsTo1Bit((COL_PIXEL / 8) * ROW_xPixel * 2 + 10, BMP);
    Col_num_7 = Combine_2BitsTo1Bit((COL_PIXEL / 8) * ROW_xPixel * 2 + 12, BMP);
    Col_num_8 = Combine_2BitsTo1Bit((COL_PIXEL / 8) * ROW_xPixel * 2 + 14, BMP);

    // delayMicroseconds(1000); 
  }
}

void setup()
{
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);

  pinMode(en_74138, OUTPUT);
  pinMode(la_74138, OUTPUT);
  pinMode(lb_74138, OUTPUT);
  pinMode(lc_74138, OUTPUT);
  pinMode(ld_74138, OUTPUT);

  digitalWrite(en_74138, LOW);

  // AVR Port Settings
  DDRD |= 0x78; //Set PIN 3 4 5 6 output  
}

unsigned int lenght_of_line(unsigned char *expression, char **matrix)
{
  unsigned int lenght = 0;
  unsigned int symbol_number = 0;

  while (unsigned char symbol = *(expression + symbol_number))
  {
    char *s_array = get_alphabete_array(expression, symbol_number, matrix);
    lenght += s_array[0];

    symbol_number++;
  };

  return lenght;
}

void clear_output_content()
{
  for (int i = 0; i < ROW_PIXEL * COL_PIXEL / 4; i++)
  {
    output_content[i] = 0;
  }
}

void fill_left_side(unsigned char *expression, unsigned int position, unsigned int first_line, unsigned int height, char **matrix)
{
  // calculate symbol
  unsigned int symbol_number = get_symbol_number(expression, position, matrix);
  char* s_array = get_alphabete_array(expression, symbol_number, matrix);

  // fill very right column

  // get symbol_width
  unsigned int symbol_width = s_array[0];
  unsigned int symbol_height = s_array[1];

  // calculate rest
  unsigned int symbol_rest = position - symbol_number * symbol_width;

  // add current position to very right side on led matrix

  // calculate start line
  unsigned int s_array_line = 0;
  for (unsigned int j = first_line; j < height + first_line; j++, s_array_line)
  {
    // calculate offset on output_content (very right byte of current line)
    unsigned int x_offset = COL_PIXEL / 4 * j; // + COL_PIXEL / 4 - 1;

    // get byte
    unsigned int s_array_offset = s_array_line * symbol_width / 4 + symbol_rest / 4;
    unsigned char symbol = s_array[2 + s_array_offset];

    s_array_line++;

    // get offset inside char

    // calculate group number
    unsigned int group_number = position / 4;

    // calculate rest inside group
    unsigned int position_inside_group = position - (group_number * 4);

    // add mask
    unsigned int shift = (position_inside_group * 2);
    unsigned char mask = symbol;
    switch (shift)
    {
    case 0:
      mask &= 0x03;
      break;
    case 2:
      mask &= 0x0C;
      break;
    case 4:
      mask &= 0x30;
      break;
    case 6:
      mask &= 0xC0;
      break;
    };
    mask = mask >> shift;

    // save byte on output_content
    unsigned char value = output_content[x_offset] | mask;
    output_content[x_offset] = value;
  }
}

void fill_right_side(unsigned char *expression, unsigned int position, unsigned int first_line, unsigned int height, char **matrix)
{
  // calculate symbol
  unsigned int symbol_number = get_symbol_number(expression, position, matrix);
  char* s_array = get_alphabete_array(expression, symbol_number, matrix);

  // fill very right column

  // get symbol_width
  unsigned int symbol_width = s_array[0];
  unsigned int symbol_height = s_array[1];

  // calculate rest
  unsigned int symbol_rest = position - symbol_number * symbol_width;

  // add current position to very right side on led matrix

  // calculate start line
  unsigned int s_array_line = 0;
  unsigned int line = first_line;
  for (unsigned int j = line; j < height + line; j++, s_array_line)
  {
    // calculate offset on output_content (very right byte of current line)
    unsigned int x_offset = COL_PIXEL / 4 * j + COL_PIXEL / 4 - 1;

    // get byte
    unsigned int s_array_offset = s_array_line * symbol_width / 4 + symbol_rest / 4;
    unsigned char symbol = s_array[2 + s_array_offset];

    s_array_line++;

    // get offset inside char

    // calculate group number
    unsigned int group_number = position / 4;

    // calculate rest inside group
    unsigned int position_inside_group = position - (group_number * 4);

    // add mask
    unsigned int shift = ((4 - position_inside_group - 1) * 2);
    unsigned char mask = symbol;
    switch (shift)
    {
    case 0:
      mask &= 0xC0;
      break;
    case 2:
      mask &= 0x30;
      break;
    case 4:
      mask &= 0x0C;
      break;
    case 6:
      mask &= 0x03;
      break;
    };
    mask = mask << shift;

    // save byte on output_content
    unsigned char value = output_content[x_offset] | mask;
    output_content[x_offset] = value;
  }
}

void shift_left(unsigned int first_line, unsigned int height)
{
  // shift left one position
  for (unsigned int y = first_line; y < first_line + height; y++)
  {
    unsigned int x1 = y * COL_PIXEL / 4;
    unsigned int x2 = x1 + COL_PIXEL / 4 - 1;

    unsigned char rest = 0;
    for (; x2 > x1; x2--)
    {
      unsigned char temp_rest = output_content[x2] & 0x03;
      temp_rest = temp_rest << 6;

      // shift pixel left
      output_content[x2] = output_content[x2] >> 2;

      // add rest of previous byte
      output_content[x2] |= rest;

      // get rest
      rest = temp_rest;
    }

    output_content[x1] = output_content[x1] >> 2;
    output_content[x1] |= rest;
  }
}

void shift_right(unsigned int first_line, unsigned int height)
{
  // shift right one position
  for (unsigned int y = first_line; y < first_line + height; y++)
  {
    unsigned int x1 = y * COL_PIXEL / 4;
    unsigned int x2 = x1 + COL_PIXEL / 4;

    unsigned char rest = 0;
    for (; x1 < x2; x1++)
    {
      unsigned char temp_rest = output_content[x1] & 0xC0;
      temp_rest = temp_rest >> 6;

      // shift pixel right
      output_content[x1] = output_content[x1] << 2;

      // add rest of previous byte
      output_content[x1] |= rest;

      // get rest
      rest = temp_rest;
    }
  }
}

unsigned int get_symbol_number(unsigned char *expression, unsigned int position, char **matrix)
{
  unsigned int symbol_number = -1;
  unsigned int current_position = 0;

  int temp_position = position;

  while (temp_position >= 0)
  {
    char* s_array = get_alphabete_array(expression, current_position++, matrix);

    // get width of symbol
    temp_position -= s_array[0];
    symbol_number++;
  };

  return symbol_number;
}

char *get_alphabete_array(unsigned char *expression, unsigned int symbol_number, char **matrix)
{
  unsigned char s = *(expression + symbol_number);

  char* s_array = 0;
  switch (s)
  {
  case 1: // right arrow
    s_array = matrix[27]; // use this for en alphabete
    // s_array = matrix[33]; // use this for ru alphabete
    break;
  case ' ': // space
	s_array = matrix[26];  // use this for ru alphabete
    // s_array = matrix[32];  // use this for ru alphabete
    break;
  default:
    s_array = matrix[s - 'a']; // use this for en alphabete
    //s_array = matrix[s - 127 - 'a']; // use this for ru alphabete
    break;
  };

  return s_array;
}

void loop()
{
  clear_output_content();
  
  {
    // launch program 1
    set_program_1();

  }

  clear_output_content();

  {
    // launch program 2
    set_program_2();
  }

  clear_output_content();

  {
    // launch program 3
    //set_program_3();
  }

  clear_output_content();

  {
    // launch program 4
    set_program_4();
  }

  clear_output_content();

  {
    // launch program 5
    set_program_5();
  }

  clear_output_content();

  {
    // launch program 6
    set_program_6();
  }

  clear_output_content();
}

void set_program_1()
{
  program_1_params *p1 = new program_1_params();

  p1->expression = program_1_expression;
  p1->position = 0;
  p1->beging_line = 4;
  p1->height = 8;

  p1->matrix = alphabete_small;

  unsigned int lenght = lenght_of_line(p1->expression, p1->matrix);

  for (unsigned int i = 0; i < lenght + 40; i++)
  {
    program_1(p1);
    display_martix(output_content);
    delay(10);
  }

  delete p1;
}

void set_program_2()
{
  program_1_params *p2 = new program_1_params();

  p2->expression = program_2_expression;
  p2->position = 0;
  p2->beging_line = 4;
  p2->height = 8;

  p2->matrix = alphabete_small;

  unsigned int lenght = lenght_of_line(p2->expression, p2->matrix);

  for (unsigned int i = 0; i < lenght + 40; i++)
  {
    program_2(p2);
    display_martix(output_content);
    delay(10);
  }

  delete p2;
}

void set_program_3()
{
  program_1_params *p3 = new program_1_params();

  p3->expression = program_3_expression;
  p3->position = 0;
  p3->beging_line = 4;
  p3->height = 8;

  p3->matrix = alphabete_small;

  unsigned int lenght = lenght_of_line(p3->expression, p3->matrix);

  for (unsigned int i = 0; i < lenght + 40; i++)
  {
    program_3(p3);
    display_martix(output_content);
    delay(10);
  }

  delete p3;
}

void set_program_4()
{
  program_4_params *p4 = new program_4_params();

  p4->expression_1 = program_4_1_expression;
  p4->position_1 = 0;
  p4->beging_line_1 = 0;
  p4->height_1 = 8;

  p4->expression_2 = program_4_2_expression;
  p4->position_2 = 0;
  p4->beging_line_2 = 8;
  p4->height_2 = 8;

  p4->matrix = alphabete_small;

  unsigned int lenght = lenght_of_line(p4->expression_1, p4->matrix);

  for (unsigned int i = 0; i < lenght + 40; i++)
  {
    program_4(p4);
    display_martix(output_content);
    delay(10);
  }

  delete p4;
}

void set_program_5()
{
  program_5_params *p5 = new program_5_params();

  p5->expression_1 = program_5_1_expression;
  p5->position_1 = 0;
  p5->beging_line_1 = 0;
  p5->height_1 = 8;
  p5->speed_1 = 1;

  p5->expression_2 = program_5_2_expression;
  p5->position_2 = 0;
  p5->beging_line_2 = 8;
  p5->height_2 = 8;
  p5->speed_2 = 2;

  p5->matrix = alphabete_small;

  unsigned int lenght = lenght_of_line(p5->expression_1, p5->matrix);

  for (unsigned int i = 0; i < lenght + 40; i++)
  {
    program_5(p5);
    display_martix(output_content);
    delay(10);
  }

  delete p5;
}

void set_program_6()
{
  program_1_params *p3 = new program_1_params();

  p3->expression = program_6_expression;
  p3->position = 0;
  p3->beging_line = 4;
  p3->height = 8;

  p3->matrix = alphabete_small;

  unsigned int lenght = lenght_of_line(p3->expression, p3->matrix);

  // shift
  for (unsigned int i = 0; i < lenght; i++)
  {
    program_3(p3);
    display_martix(output_content);
    delay(10);
  }

  delete p3;

  delay(500);

  program_6_params *p6 = new program_6_params();
  p6->expression = program_6_expression;
  p6->blink = true;

  p6->beging_line = 4;
  p6->height = 8;

  p6->matrix = alphabete_small;

  // blink
  for (unsigned int i = 0; i < 10; i++)
  {
    program_6(p6);

    int repeater = 50;
    while (repeater-- > 0)
    {
      display_martix(output_content);
      delay(5);
    };

    p6->blink = !p6->blink;
  }

  delete p6;
}

// 1. running line left-right big font
void program_1(program_1_params* p)
{
  unsigned int lenght = lenght_of_line(p->expression, p->matrix);

  if (p->position >= lenght)
  {
    shift_left(p->beging_line, p->height);
    if (p->position >= (lenght + 30))
    {
      p->position = 0;
    }
  }
  else
  {
    shift_left(p->beging_line, p->height);
    fill_right_side(p->expression, p->position, p->beging_line, p->height, p->matrix);
  }

  p->position += 1;
}

// 2. ------------ right-left big font
void program_2(program_1_params *p)
{
  unsigned int lenght = lenght_of_line(p->expression, p->matrix);

  if (p->position >= lenght)
  {
    shift_right(p->beging_line, p->height);
    if (p->position >= lenght)
    {
      p->position = 0;
    }
  }
  else
  {
    shift_right(p->beging_line, p->height);
    fill_left_side(p->expression, p->position, p->beging_line, p->height, p->matrix);
  }

  p->position += 1;
}

// 3. ------------ left-right small font
void program_3(program_1_params *p)
{
  unsigned int lenght = lenght_of_line(p->expression, p->matrix);

  if (p->position >= lenght)
  {
    shift_left(p->beging_line, p->height);
    if (p->position >= lenght)
    {
      p->position = 0;
    }
  }
  else
  {
    shift_left(p->beging_line, p->height);
    fill_right_side(p->expression, p->position, p->beging_line, p->height, p->matrix);
  }

  p->position += 1;
}

// 4. ------------ left-right two lines small font
void program_4(program_4_params *p)
{
  // upper line
  unsigned int lenght1 = lenght_of_line(p->expression_1, p->matrix);

  if (p->position_1 >= lenght1)
  {
    shift_left(p->beging_line_1, p->height_1);
    if (p->position_1 >= lenght1)
    {
      p->position_1 = 0;
    }
  }
  else
  {
    shift_left(p->beging_line_1, p->height_1);
    fill_right_side(p->expression_1, p->position_1, p->beging_line_1, p->height_1, p->matrix);
  }

  // lower line
  unsigned int lenght2 = lenght_of_line(p->expression_2, p->matrix);

  if (p->position_2 >= lenght2)
  {
    shift_right(p->beging_line_2, p->height_2);
    if (p->position_2 >= lenght2)
    {
      p->position_2 = 0;
    }
  }
  else
  {
    shift_right(p->beging_line_2, p->height_2);
    fill_left_side(p->expression_2, p->position_2, p->beging_line_2, p->height_2, p->matrix);
  }

  p->position_1 += 1;
  p->position_2 += 1;
}

// 5. ------------ left-right two lines with different speeds
void program_5(program_5_params *p)
{
  // upper line
  unsigned int lenght1 = lenght_of_line(p->expression_1, p->matrix);

  if (p->position_1 >= lenght1)
  {
    shift_left(p->beging_line_1, p->height_1);
    if (p->position_1 >= lenght1)
    {
      p->position_1 = 0;
    }
  }
  else
  {
    shift_left(p->beging_line_1, p->height_1);
    fill_right_side(p->expression_1, p->position_1, p->beging_line_1, p->height_1, p->matrix);
  }

  // lower line
  unsigned int lenght2 = lenght_of_line(p->expression_2, p->matrix);

  if (p->position_2 >= lenght2)
  {
    shift_left(p->beging_line_2, p->height_2);
    if (p->position_2 >= lenght2)
    {
      p->position_2 = 0;
    }
  }
  else
  {
    shift_left(p->beging_line_2, p->height_2);
    fill_right_side(p->expression_2, p->position_2, p->beging_line_2, p->height_2, p->matrix);
    shift_left(p->beging_line_2, p->height_2);
    fill_right_side(p->expression_2, p->position_2, p->beging_line_2, p->height_2, p->matrix);
  }

  p->position_1 += 1;
  p->position_2 += 1;
}

// 6. ------------ big font shift three zones (left-right, right-left, leftright)
void program_6(program_6_params *p)
{
  if (p->blink)
  {
    unsigned int position = 0;
    int lenght = lenght_of_line(p->expression, p->matrix);
    while (lenght-- > 0)
    {
      fill_right_side(p->expression, position++, p->beging_line, p->height, p->matrix);
      shift_left(p->beging_line, p->height);
    };
  }
  else
  {
    clear_output_content();
  }
}






