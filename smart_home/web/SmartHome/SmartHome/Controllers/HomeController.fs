﻿namespace SmartHome.Controllers

open System
open System.Collections.Generic
open System.Linq
open System.Threading.Tasks
open Microsoft.AspNetCore.Mvc
open System.IO.Ports

[<Route("api/[controller]")>]
[<ApiController>]
type HomeController () =
    inherit ControllerBase()

    [<HttpGet>]
    member this.Get() =
        let values = [|"value1"; "value2"|]
        ActionResult<string[]>(values)

    [<HttpGet("{id}")>]
    member this.Get(id:int) =
        let value = "value"
        ActionResult<string>(value)

    [<HttpPost>]
    member this.Post([<FromBody>] value:string) =
        ()

    [<HttpPut("{id}")>]
    member this.Put(id:int, [<FromBody>] value:string ) =
        ()

    [<HttpDelete("{id}")>]
    member this.Delete(id:int) =
        ()

    [<HttpGet("reles/{reles}")>]
    /// reles/1,2,3,4,5,6,7,8
    member this.Get(reles:string) =

        let rele_data = [0b11111110; 0b11111101; 0b11111011; 0b11110111; 0b11101111; 0b11011111; 0b10111111; 0b01111111]

        let (|ItsInt|Fail|) (value:String) =
            let success, result = Int32.TryParse(value)
            if success && result > 0 && result < rele_data.Length then
                ItsInt(result-1)
            else Fail

        let values =
            reles.Split ','
            |> Array.map (fun x ->
                match x with
                    | ItsInt i -> Some(i)
                    | Fail -> None
                )
            |> Array.filter Option.isSome
            |> Array.fold (fun a x -> a &&& rele_data.[x.Value]) 0b11111111

        let command = 
            sprintf "%d" values

        let port = new SerialPort(PortName="COM6", BaudRate=9600, DataBits=8, Parity=Parity.None, StopBits=StopBits.One);

        port.Open()
        port.WriteLine command
        port.Close()

        ActionResult<string>(command)