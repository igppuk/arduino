int dataPin  = 2;
int clockPin = 3;
int latchPin = 4;

//byte rele_1 = B11111110;
//byte rele_2 = B11111101;
//byte rele_3 = B11111011;
//byte rele_4 = B11110111;
//byte rele_5 = B11101111;
//byte rele_6 = B11011111;
//byte rele_7 = B10111111;
//byte rele_8 = B01111111;

void setup() {

  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);

  Serial.begin(9600);
  delay(1000);
  Serial.println("ARDUINO RELES STARTED...");
}

void loop() {
//    Serial.println("Tick");

//    digitalWrite(latchPin, LOW);
//    shiftOut(dataPin, clockPin, LSBFIRST, B00000000);
//    digitalWrite(latchPin, HIGH);
//    delay(1000);
//
//    digitalWrite(latchPin, LOW);
//    shiftOut(dataPin, clockPin, LSBFIRST, B11111110);
//    digitalWrite(latchPin, HIGH);
//    delay(1000);

    // send data only when you receive data:
    if (Serial.available() > 0) {
      String str = Serial.readString();
      int data = str.toInt();
      digitalWrite(latchPin, LOW);
      shiftOut(dataPin, clockPin, LSBFIRST, data);
      digitalWrite(latchPin, HIGH);
//      Serial.println("received: " + String(data));
    }


    /*digitalWrite(latchPin, LOW);
    shiftOut(dataPin, clockPin, LSBFIRST, rele_1);
    digitalWrite(latchPin, HIGH);
    delay(500);

    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, clockPin, LSBFIRST, rele_2);
    digitalWrite(latchPin, HIGH);
    delay(500);

    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, clockPin, LSBFIRST, rele_3);
    digitalWrite(latchPin, HIGH);
    delay(500);

    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, clockPin, LSBFIRST, rele_4);
    digitalWrite(latchPin, HIGH);
    delay(500);

    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, clockPin, LSBFIRST, rele_5);
    digitalWrite(latchPin, HIGH);
    delay(500);

    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, clockPin, LSBFIRST, rele_6);
    digitalWrite(latchPin, HIGH);
    delay(500);

    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, clockPin, LSBFIRST, rele_7);
    digitalWrite(latchPin, HIGH);
    delay(500);

    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, clockPin, LSBFIRST, rele_8);
    digitalWrite(latchPin, HIGH);
    delay(500);*/
}
